function MoviesApiService() {
  const BASE_URL = 'https://api.themoviedb.org/3/';
  const xmlhttp = new XMLHttpRequest();

  this.getMovies = (query = '',
                    onSuccessCB = () => {},
                    onErrCB = () => {},
                    onLoadingCB = () => {}) => {

    prepareXmlhttpInstance(
      {
        httpMethod: "GET",
        url: addParamsToUrl(
          `${BASE_URL}search/movie`,
          {
            ...getBaseParamsForReq(),
            query
          }
        ),
        headers: [
          {
            key: "Accept",
            value: "application/json"
          }
        ],
        onSuccessCB,
        onErrCB,
        onLoading: onLoadingCB
      },
      xmlhttp);

    return xmlhttp.send();
  };
}
