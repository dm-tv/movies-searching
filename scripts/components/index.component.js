function IndexComponent() {
  // available dom elements
  const searchInput = document.getElementById('searchInput');
  const loadingDiv = document.getElementById('loadingDiv');
  const moviesUl = document.getElementById('moviesUl');

  const moviesApiService = new MoviesApiService();

  const baseStorageName = 'IndexComponent::';
  const availableStoragesKeys = {
    moviesRes: baseStorageName + 'moviesRes',
    searchInput: baseStorageName + 'searchInput'
  };

  const initSubscriptions = () => {

    searchInput.addEventListener('input', function () {
      if (inputValidation(this.value)) {
        moviesApiService.getMovies(
          this.value,
          (data) => {
            saveDataToStorage(data, availableStoragesKeys.moviesRes);
            saveDataToStorage(this.value, availableStoragesKeys.searchInput);

            renderData(data);
            loadingDiv.style.visibility = 'hidden';
          },
          (err) => {
            console.log(err);
            loadingDiv.style.visibility = 'hidden';
          },
          () => {
            loadingDiv.style.visibility = 'visible';
          })
      }
    });
  };

  const inputValidation = (inputValue) => {
    const minLengthValidation = (v) => {
      const minLength = 3;

      return v.length >= minLength;
    };

    const validations = [
      minLengthValidation
    ];

    return validations.every((validationFunc) =>
      validationFunc(inputValue));
  };

  const renderData = (data) => {
    let moviesTemplate = '';

    if (data.results.length !== 0) {
      for (let i = 0; i < data.results.length; i++) {
        moviesTemplate += getMovieTemplate(data.results[i]);
      }
    } else {
      moviesTemplate += `<li><h2>Nothing to show</h2></li>`;
    }


    moviesUl.innerHTML = moviesTemplate;
  };

  const getMovieTemplate = function (movie) {
    const imageBaseUrl = 'https://image.tmdb.org/t/p/w300_and_h450_bestv2/';
    const movieImageUrl = movie.poster_path
      ? `${imageBaseUrl}${movie.poster_path}`
      : "http://i.piccy.info/i9/c89ddd1352404af087ee938569f47f96/1547207329/1749/1294338/oie_PRn7N7rqAyw9.png";

    return `
            <li>
            <figure>
                <figcaption><h3>${movie.title}</h3></figcaption>
                <img src="${movieImageUrl}" alt="">
            </figure>
            <div class="overview">
                <h3>Overview</h3>
                <p>
                    ${movie.overview}
                </p>
            </div>
        </li>
    `
  };

  this.initComponent = () => {

    const searchInputValue = getDataFromStorage(availableStoragesKeys.searchInput);
    const moviesResValue = getDataFromStorage(availableStoragesKeys.moviesRes);

    if (searchInputValue && moviesResValue) {
      renderData(moviesResValue);
      searchInput.value = searchInputValue;
    }

    initSubscriptions();
  }

}

