/*
Add Search functionality to this page:
- use moviedb api for movie data, key is: 0adbb34bf81e230a73e19aaaeee72637 
    (https://www.themoviedb.org/documentation/api?language=en-US) ;
- list should be updated on the go (while user is typing), shortest search string is 3 characters;
- if search result is empty you need to show appropriate message, for example "Nothing to show”;
- you should visually reflect loading proces, for example show "loading…” message.

Important: 
- no redundant requests, use debunce or cancelation where appropriate. 
- use caching for previos requests
- no frameworks. Native(Vanilla) Javascript only.
*/


