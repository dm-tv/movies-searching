function saveDataToStorage (data, key) {
  localStorage.setItem(key, JSON.stringify(data));
}

function getDataFromStorage(key) {
  return localStorage.getItem(key) ? JSON.parse(localStorage.getItem(key)) : null;
}