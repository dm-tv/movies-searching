function addParamsToUrl(url, params) {

  const paramsArr = Object.keys(params).map((p) => {
    return {key: p, value: params[p]};
  });

  return paramsArr.reduce((acc, cv, index) => {
    const res = `${cv.key}=${cv.value}`;

    return index === 0
      ? `${acc}?${res}`
      : `${acc}&${res}`

  }, url)
}

/**
 * @param options: {{
   * url: string,
   * httpMethod: 'GET' | 'POST' | 'DELETE' | 'PUT',
   * headers: {key: string, value: any}[],
   * onSuccessCB: Function,
   * onErrCB: Function
   * onLoading: Function
   * }}
 */
prepareXmlhttpInstance = function (options, xmlhttpInstance) {
  if (xmlhttpInstance.readyState !== 4) {
    xmlhttpInstance.abort();
  }

  xmlhttpInstance.open(options.httpMethod, options.url, true);

  for (let i = 0; i < options.headers.length; i++) {
    xmlhttpInstance.setRequestHeader(options.headers[i].key, options.headers[i].value);
  }

  xmlhttpInstance.onreadystatechange = () => {
    if (xmlhttpInstance.readyState === 4) {
      const res = xmlhttpInstance.responseText.length !== 0
        ? JSON.parse(xmlhttpInstance.responseText)
        : null;

      if (xmlhttpInstance.status === 200) {
        options.onSuccessCB(res);
      }
      else {
        options.onErrCB(res);
      }
    } else {
      options.onLoading();
    }
  };
};

function getBaseParamsForReq() {
  return {
    api_key: '0adbb34bf81e230a73e19aaaeee72637',
    language: 'en-US'
  }
}

